/*
Copyright (C) 2021, George Zeakis <zeageorge@gmail.com> FORTH-ICS

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License version 3
as published by the Free Software Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.zeageorge.pdfproperties;

import com.beust.jcommander.IParameterValidator;
import com.beust.jcommander.ParameterException;

/**
 *
 * @author George Zeakis <zeageorge@gmail.com>
 */
public class PositiveIntegerValidator implements IParameterValidator {
  /**
   * Validate the parameter.
   *
   * @param name  The name of the parameter (e.g. "-host").
   * @param value The value of the parameter that we need to validate
   *
   * @throws ParameterException Thrown if the value of the parameter is invalid.
   */
  public void validate(String name, String value) throws ParameterException {
    ParameterException parameterException = new ParameterException("Parameter " + name + " should be positive (found " + value + ")");
    try {
      int n = Integer.parseInt(value);
      if (n < 0) {
        throw parameterException;
      }
    } catch (NumberFormatException e) {
      throw parameterException;
    }
  }
}
