/*
Copyright (C) 2021, George Zeakis <zeageorge@gmail.com> FORTH-ICS

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License version 3
as published by the Free Software Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.zeageorge.pdfproperties;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;
import com.beust.jcommander.ParameterException;
import java.io.FileOutputStream;
import java.io.IOException;

import com.itextpdf.text.DocumentException;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfStamper;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.regex.PatternSyntaxException;

/**
 *
 * @author George Zeakis <zeageorge@gmail.com>
 */
public class PDFProperties {
  private static final String COPYRIGHT_HEADER = 
        "--------------------------------------------------------------\n"
      + " PDFProperties v1.0.0 20210716                                \n"
      + " Copyright 2021 George Zeakis <zeageorge@gmail.com> FORTH-ICS \n"
      + " LICENSE GNU Affero General Public License v3.0               \n"
      + "--------------------------------------------------------------\n";

  private static final String EXAMPLES = "\n  Examples:                                            \n"
      + " $ java -jar PDFProperties.jar -i file.pdf -o outfile.pdf -set \"key_name=value\"\n"
      + " $ java -jar PDFProperties.jar -i file.pdf -o outfile.pdf -set \"key1=value 1\" -set key2=value2 -get key3\n"
      + " $ java -jar PDFProperties.jar -i file.pdf -get key1 -get key2\n"
      + " $ java -jar PDFProperties.jar -i file.pdf -get_all\n\n" + " info:\n"
      + " to remove a property use -set \"key_name\" -o outfile.pdf\n"
      + " each property can hold up to 256 characters for its value\n" + " \n\n";

  private static final String PROGRAM_NAME = "PDFProperties";

  @Parameter(names = { "-i" }, required = true, description = "Input pdf file", order = 1)
  String infilename;

  @Parameter(names = { "-o" }, description = "Output pdf file in case of setting a property", order = 2)
  String outfilename;

  @Parameter(names = { "-set" }, description = "set a key=value pair", order = 3)
  private List<String> sets = new ArrayList<>();

  @Parameter(names = { "-get" }, description = "get the value of a given keyname", order = 4)
  private List<String> gets = new ArrayList<>();

  @Parameter(names = "-get_all", description = "Get all properties")
  private boolean get_all = false;

  @Parameter(names = { "--help", "-h" }, description = "Help/Usage", help = true, order = 1024)
  boolean help;

  public static void main(String[] args) {
    System.out.print(COPYRIGHT_HEADER);
    try {
      PDFProperties app = new PDFProperties();
      JCommander jCommander = JCommander.newBuilder().addObject(app).build();
      jCommander.setProgramName(PROGRAM_NAME);
      jCommander.parse(args);
      app.run(jCommander);
    } catch (ParameterException e) {
      e.usage();
      System.out.println(e.getMessage());
    }
  }

  public void run(JCommander jCommander) {
    if (help) {
      jCommander.usage();
      System.out.println(EXAMPLES);
      return;
    }

    try {
      PdfReader reader = new PdfReader(infilename);
      Map<String, String> info = reader.getInfo();
      if (get_all) {
        info.entrySet().forEach((entry) -> {
          String key = entry.getKey();
          String value = entry.getValue();
          System.out.println(key + "=" + value);
        });
      }

      for (String key_name : gets) {
        if (info.containsKey(key_name)) {
          System.out.println(key_name + "=" + String.valueOf(info.get(key_name)));
        }
      }

      if (!sets.isEmpty()) {
        if (null == outfilename || outfilename.isEmpty()) {
          System.out.println("Output file must be set");
          System.exit(1);
          reader.close();
        }
        PdfStamper stamper = new PdfStamper(reader, new FileOutputStream(outfilename), '\0', true);
        for (String pair_str : sets) {
          if (pair_str.contains("=")) {
            String[] pair = pair_str.split("=", 2);
            if (pair.length == 2 && !"=".equals(pair[0])) {
              info.put(pair[0], pair[1]);
              stamper.setMoreInfo(info);
            }
          } else {
            info.put(pair_str, null);
            stamper.setMoreInfo(info);
          }
        }
        
        stamper.close();
      }
      reader.close();
    } catch (IOException | DocumentException | ClassCastException | NullPointerException | PatternSyntaxException | UnsupportedOperationException e) {
      e.printStackTrace();
      System.exit(1);
    }
  }
}
