<?php

declare(strict_types=1);

/*
Copyright (C) 2021, George Zeakis <zeageorge@gmail.com> FORTH-ICS

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License version 3
as published by the Free Software Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace app\examples\php;

use Throwable;

require './PDFPropertiesManager.php';

$java_bin_path = 'java'; // '/usr/bin/java';
$pdfproperties_file_path = __DIR__ . '/../../PDFProperties.jar';

$pdf_file = 'test.pdf';

$pdfPropertiesManager = new PDFPropertiesManager($java_bin_path, $pdfproperties_file_path);

try {
  $pdfPropertiesManager->setProperty($pdf_file, 'key1', 'value1');

  echo 'key1:' . $pdfPropertiesManager->getProperty($pdf_file, 'key1');

  echo "\n";

  echo 'key2:' . $pdfPropertiesManager->getProperty($pdf_file, 'key2', 'not found');
} catch (Throwable $t) {
  echo $t->getMessage();
}
