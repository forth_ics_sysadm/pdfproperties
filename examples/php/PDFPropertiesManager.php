<?php

declare(strict_types=1);

/*
Copyright (C) 2021, George Zeakis <zeageorge@gmail.com> FORTH-ICS

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License version 3
as published by the Free Software Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace app\examples\php;

use Exception;
use function 
  exec,
  count,
  putenv,
  rename,
  explode,
  setlocale,
  json_encode;
use const 
  JSON_UNESCAPED_UNICODE, 
  JSON_UNESCAPED_SLASHES;

/**
 * Description of PDFPropertiesManager
 *
 * @author George Zeakis <zeageorge@gmail.com>
 * @license GNU Affero General Public License version 3
 */
class PDFPropertiesManager {
  const LOCALE_GR = "el_GR.UTF-8";
  
  protected $java_bin_path;
  protected $pdfproperties_file_path;

  /**
   * 
   * @param string $java_bin_path default value is '/usr/bin/java'
   * @param string $pdfproperties_file_path default value is 'PDFProperties.jar'
   */
  public function __construct(string $java_bin_path = '/usr/bin/java', string $pdfproperties_file_path = 'PDFProperties.jar') {
    $this->java_bin_path = $java_bin_path;

    $this->pdfproperties_file_path = $pdfproperties_file_path;
  }

  /**
   * 
   * @param string $pdf_file The full path to the PDF file
   * @param string $name
   * @param mixed $default
   * @return string
   * @throws Exception
   * @todo you should redirect stderr to stdout
   */
  public function getProperty(string $pdf_file, string $name, $default = null): string {
    $exec_output = [];
    $exec_return_value = 0;

    $exec_command = "{$this->java_bin_path} -jar \"{$this->pdfproperties_file_path}\" -i \"{$pdf_file}\" -get \"{$name}\"";

    setlocale(LC_ALL, self::LOCALE_GR);
    putenv('LC_ALL=' . self::LOCALE_GR);

    $exec_result = exec($exec_command, $exec_output, $exec_return_value);

    if ($exec_return_value != 0) {
      throw new Exception(json_encode([
          'exec_command' => $exec_command,
          'error' => [
            'message' => "Execution of [{$this->pdfproperties_file_path}] failed with exit code [{$exec_return_value}]",
            'exec_return_value' => $exec_return_value,
            'exec_output' => $exec_output,
            'exec_result' => $exec_result,
          ],
        ], JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES));
    }

    $property_value = $default;

    if (count($exec_output) > 5) {
      list($pname, $property_value) = explode('=', $exec_output[5]);
    }

    return $property_value;
  }

  /**
   * 
   * @param string $pdf_file The full path to the PDF file
   * @param string $name
   * @param string $value
   * @return void
   * @throws Exception
   * @todo you should redirect stderr to stdout
   */
  public function setProperty(string $pdf_file, string $name, string $value) {
    $changed_file = $pdf_file . ".changed.pdf";

    $exec_output = [];
    $exec_return_value = 0;

    $exec_command = "{$this->java_bin_path} -jar {$this->pdfproperties_file_path} -i \"{$pdf_file}\" -o \"{$changed_file}\" -set \"{$name}\"=\"{$value}\"";
    
    setlocale(LC_ALL, self::LOCALE_GR);
    putenv('LC_ALL=' . self::LOCALE_GR);

    $exec_result = exec($exec_command, $exec_output, $exec_return_value);

    if ($exec_return_value != 0) {
      throw new Exception(json_encode([
        'exec_command' => $exec_command,
        'error' => [
          'message' => "Execution of [{$this->pdfproperties_file_path}] failed with exit code [{$exec_return_value}]",
          'exec_return_value' => $exec_return_value,
          'exec_output' => $exec_output,
          'exec_result' => $exec_result,
        ],
      ], JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES));
    }

    rename($changed_file, $pdf_file);
  }

}